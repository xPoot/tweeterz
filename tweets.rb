# --- Tweets.rb ---
# Applikation för att ta reda på vem som mentionat en specifik användare mest på twitter.

require 'sinatra'
require 'twitter'

# Funktion för att plocka ut den key+value i hashen med högst value.
def largest_hash_key(hash)
	hash.max_by{ |k, v| v}
end

# Fånga alla followers från en användare i en array
def get_followers(username)
	followers_from_hash = Twitter.followers(username)
	followers_array = []
	followers_from_hash.users.each do |f|
		followers_array << f.screen_name
	end
	return followers_array
end

# Skapar en hash med Keys för varje användare med deras mentions som value
def count_follower_mentions(username, followers)
	users = Hash.new
	mentions = []
	followers.each do |follower|
		Twitter::Search.new.containing(username).from(follower).each do |r|
			mentions << r.from_user
		end
		users[follower] = mentions.length
		mentions = []
	end
	return users
end

get '/' do
	erb :home
end

post '/' do
	
	@user = params[:user]

	# Funktioner för att lägga in followers och mentions i en hash
	followers = get_followers(@user)
	users = count_follower_mentions(@user, followers)

	# Plockar den användare ur hashen med mest mentions och lägger i en array
	@get_largest_value = largest_hash_key(users)

	# Lägger användare och antalet mentions i en array, formaterar text.
	@get_largest_value[0] = @get_largest_value[0].capitalize
	@user = @user.capitalize
	
	# Call till View
	erb :output

end